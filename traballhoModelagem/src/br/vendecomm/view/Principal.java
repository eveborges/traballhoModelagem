package br.vendecomm.view;
import br.vendecomm.control.Controladora;
import br.vendecomm.model.Endereco;

public class Principal {
	
	public static void main(String[] args) {	
		Controladora controladora = Controladora.getInstance();
		controladora.cadastrarProduto(1, "DiscoVoador", 100, 487);
		controladora.cadastrarCliente("00055566698", "Chaves", "985446632", "01/01/2000",Endereco.RESIDENCIAL);
		
		for(String srt : controladora.selecionarProduto()) {
			System.out.println(srt);
		}
		for (String str : controladora.selecionarCliente()){
			System.out.println(str);
		}
		
		controladora.selecionarNomeCliente("Chaves");
		controladora.carregarCompra(1);
		controladora.venderProduto(1, "DiscoVoador", 1, 10);
	}
	
}
