package br.vendecomm.control;

import java.util.ArrayList;
import java.util.List;

import br.vendecomm.model.Cliente;
import br.vendecomm.model.Compra;
import br.vendecomm.model.Endereco;
import br.vendecomm.model.ItemCompra;
import br.vendecomm.model.Produto;

public class Controladora {
	
	private static Controladora controle = null;
	private Cliente cliente = null;
	private List<Produto> cadastrarProduto = new ArrayList<Produto>();
	private List<Cliente> cadastrarCliente = new ArrayList<Cliente>();
	
	
	private Controladora(){}
	
	public static Controladora getInstance(){
		if(controle == null){
			return controle;
		} else throw new RuntimeException("Criado com sucesso!!");
	}
	
	
	public void cadastrarProduto(int codigo,String descricaoProduto, int quantidade, double preco){
		Produto prod = new Produto(codigo, descricaoProduto);
				
		for(Produto produto: cadastrarProduto){
			if(produto.getDescricaoProduto().equals(descricaoProduto)){
				cadastrarProduto.add(prod);
				break;
			}
		}
	}
	
	
	public List<String> selecionarProduto(){
		List<String> produtos = new ArrayList<String>();
		
		for(Produto produto:cadastrarProduto){
			produtos.add(produto.getDescricaoProduto());
		}
		
		return produtos;
	}
	
	public void cadastrarCliente(String cpf, String nome, String telefone, String dataCadastro, Endereco tipo ){
		Cliente cli = new Cliente(cpf,nome,telefone,dataCadastro, tipo);
		boolean achouCliente = false;
		
		for(Cliente cliente: cadastrarCliente){
			if (cliente.getNome().equals(nome)){
				achouCliente = true;
				break;
			}
			
		}
		if(! achouCliente){
			cadastrarCliente.add(cli);
		}		
	}
	
	public List<String> selecionarCliente (){
		List<String> clientes = new ArrayList<String>();
		for(Cliente cliente: cadastrarCliente){
			clientes.add(cliente.getNome());
		}
		return clientes;
	}
	
	public void carregarCompra(int quantidade){
		Compra comp = new Compra(quantidade);
		
		if( cliente  != null ){
			cliente.adicionarCompra(comp);
			
		}else throw new RuntimeException("Cliente n�o selecionado!!");
	}
	
	public void selecionarNomeCliente(String nome) {
		
		for(Cliente cli : cadastrarCliente) {
			
			if(cli.getNome().equals(nome)) {
				cliente =cli;
			}
		}
	}
	
	public void venderProduto(int numero,String descricaoProduto, int quantidade, float valor) {
		if(cliente  != null) {
			
			Compra compraAtual = cliente .pegarCompraPorNumero(numero);
			if(compraAtual != null) {
				for(Produto produto: cadastrarProduto) {
					if(produto.getDescricaoProduto().equals(descricaoProduto)) {
						compraAtual.addItemCompra(new ItemCompra(descricaoProduto,quantidade, valor));
					}
				}
			
			}else  throw new RuntimeException("Compra n�o foi aberta!!");
		
		}else throw new RuntimeException("Cliente n�o foi selecionado!!");
	}
	
	public double totalizarVenda(int numeroCompra){
		double total=0;
		if(cliente  != null){
			Compra compraAtual = cliente .pegarCompraPorNumero(numeroCompra);
			total = compraAtual.itensTotal();
		}	else throw new RuntimeException("Compra n�o foi aberta!!");
	return total;
}
}