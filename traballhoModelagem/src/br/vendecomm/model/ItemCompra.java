package br.vendecomm.model;

public class ItemCompra {
 
	private int codigo;
	private String descricao;
	private int quantidade;
	private float preco;
	private Compra compra;
	private Produto produto;
	
	public ItemCompra(String descricao, int quantidade, float valor) {
		produto.setDescricaoProduto(descricao);
		this.setPreco(valor);
		this.quantidade = quantidade;

	}
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public float getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(float preco) {
		this.preco = preco;
	}

	public Compra getCompra() {
		return compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	 
}
 
