package br.vendecomm.model;

import java.util.Date;

public class Produto {
 
	private int codigo;
	private String descricao;
	private float qtdEstoque;
	private Date dataCadastro;
	
	public Produto(int codigo,String descricao) {
		this.codigo=codigo;
		this.descricao = descricao;
	}
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricaoProduto() {
		return descricao;
	}

	public void setDescricaoProduto(String descricao) {
		this.descricao = descricao;
	}

	public float getQtdEstoque() {
		return qtdEstoque;
	}

	public void setQtdEstoque(float qtdEstoque) {
		this.qtdEstoque = qtdEstoque;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	 
	public void manterProduto() {
	 
	}
	 
	public void consultarEstoque() {
	 
	}
	 
	public void atualizarEstoque() {
	 
	}
	 
}
 
